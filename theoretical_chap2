Name: Nguyen Minh Truong
SID: 20164384
Class: ICT.02
Question 1: If a client and a server are placed far apart, we may see network latency dominating overall performance. How can we tackle this problem? 
This problem can be solved by: 
+ client-side code can be divided into tasks which can run individually. Therefore, when one task is waiting for the server to respond, we can schedule another task
+ client can be scheduled to do other task after  a request to the server
Question 2: What is a three-tiered client-server architecture? 
A three-tiered client-server architecture consists of three logical layers, where each layer is, in principle, implemented at a separate machine. The highest layer consists of a client user interface, the middle layer contains the actual application, and the lowest layer implements the data that are being used.
Question 3: What is the difference between a vertical distribution and a horizontal distribution? 
Vertical distribution
+ Increased security by adding measures such as firewalls at different layers.
+ There would be more layers, it would take more time taken for processing meaning there would be network latency.
+ System that have vertical distributed would be susceptible in terms of reliability if there was to be an issue
+ Can add new features to the system and implement new functionalities. Ex: proxy, caches, web server etc.
Horizontal distribution
+ There are some instances which might be compromises to the security of the system.
+ Since the distribution is done within the layer itself we can reduce network latency.
+ Systems that have horizontally distributed would be more reliable as if one of the resources fails there would be others available to fill in the gaps.
+ Can improve the performance of the system for example using concepts such as replication.
Question 4: In a structured overlay network, messages are routed according to the topology of the overlay. What is an important disadvantage of this approach? 
“An overlay network is a virtual network built on top of a physical (underlay) network.” While nodes in underlay network are essentially routers but in overlay network node may be a router, a host, a server or even an application. When a message is routed across such network the shortest path between source and destination is chosen. But this logical shortest path many not be the physical best path. The source and receivers may be logically next to each other but physically farthest from each other in the network.  So it can unnecessarily delay the message delivery even the other better path is available.  
Question 5: Consider a chain of processes P1 , P2 , ..., P n implementing a multitiered  client-server architecture. Process Pi is client of process Pi+1, and Pi will  return a reply to Pi−1 only after receiving a reply from Pi+1 . What are the main problems with this organization when taking a look at the request-reply performance at process P1? 
Performance can be expected to be bad for large n. The problem is that each communication between two successive layers is, in principle, between two different machines. Consequently, the performance between P1 and P2 may also be determined by n −2 request-reply interactions between the other layers. Another problem is that if one machine in the chain performs badly or is even temporarily unreachable, then this will immediately degrade the performance at the highest level.
Question 6: Considering that a node in CAN knows the coordinates of its immediate neighbors, a reasonable routing policy would be to forward a message to the closest node toward the destination. How good is this policy? 
There are several possibilities, but if we want to follow the shortest path according to a Euclidean distance, we should follow the route (0.2,0.3) -> (0.6,0.7) -> (0.9,0.6), which has a distance of 0.882. The alternative route (0.2,0.3) -> (0.7,0.2) -> (0.9,0.6) has a distance of 0.957.
Question 7: What are the benefits of Microservices architecture compared to monolithic architecture?
+Independent components. Firstly, all the services can be deployed and updated independently, which gives more flexibility. Secondly, a bug in one microservice has an impact only on a particular service and does not influence the entire application. Also, it is much easier to add new features to a microservice application than a monolithic one.
+Easier understanding. Split up into smaller and simpler components, a microservice application is easier to understand and manage. You just concentrate on a specific service that is related to a business goal you have.
+Better scalability. Another advantage of the microservices approach is that each element can be scaled independently. So the entire process is more cost- and time-effective than with monoliths when the whole application has to be scaled even if there is no need in it. In addition, every monolith has limits in terms of scalability, so the more users you acquire, the more problems you have with your monolith. Therefore, many companies, end up rebuilding their monolithic architectures.
Question 8: Design yourself an e-commerce system using Microservices architecture.
UI<->API<-><Microservice<->DB>;<Microservice<->DB>;<Microservice<->DB>;<Microservice<->DB>


